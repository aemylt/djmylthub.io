---
layout: post
section-type: post
title: Presentation on Complexity Theory
category: Presentations
tags: [ 'complexity theory', 'presentations' ]
---

As part of this year of training for the Quantum Engineering CDT, each student has been asked to give a 1-2 hour presentation on an approved subject of our choice. I gave my presentation in mid-December, on the topic of complexity theory, and after weeks of exam revision and my Linux Mint partition breaking, I have finally been able to upload the slides for this talk.

The slides alone are available <a href="/topics_presentation/topics_presentation.pdf">here</a>, and the LaTeX source code for the slides can be found on GitHub <a href="https://github.com/djmylt/topics_presentation" target="blank">here</a>.

There were also a few points I have been thinking about since the presentation, which I want to go into more detail in this blog post. Some of these are based on questions asked in the presentation, and others are just things I think I could have explained better.

<h2>What have <span style="text-decoration:line-through;">The Romans</span> Church and Levin ever done for us?</h2>

While I described the theoretical works of Alan Turing and Stephen Cook in the presentation, I didn't give any justification for why the results are called the <em>Church</em>-Turing Thesis and the Cook-<em>Levin</em> Theorem.

Both of these titles are from a common issue within academia of multiple researchers working on the same problems independently. Alonzo Church was working on the Entscheidungsproblem at the same time as Alan Turing, but using the notation of \\(\lambda\\)-calculus instead of Turing machines. Likewise, Leonid Levin worked on finding NP-Complete problems at the same time as Stephen Cook, proposing six <a href="https://rjlipton.wordpress.com/2011/03/14/levins-great-discoveries/" target="blank">universal search problems</a> alongside Cook's.

Turing later provided a proof that Turing machines could be seen as equivalent to \\(\lambda\\)-calculus, and eventually submitted his PhD thesis to Princeton University under the supervision of Church.

<h2>Time complexity of non-halting machines</h2>

One of the first questions I was asked was about the worst-case time complexity of a machine which doesn't halt on a certain input. Is there some notation such as \\(O(\infty)\\) that we would use in that case?

Such notation can be used, but is very rare and debatable. It is far more common to simply state that the machine does not halt on some inputs, or only focus on the inputs for which the machine does halt.

<h2>Big polynomials</h2>

This was in response to my definitions of \\(\text{P}\\) and \\(\text{NP}\\) as problems that scale efficiently as the input becomes large. The problem is that this is defined for <em>any</em> polynomial; both \\(O(n^2)\\) and \\(O(n^{100,000,000,000})\\) are seen as scaling efficiently, even though the latter will grow at a vastly faster rate than the former.

This definition exists as more of a formality than something people actually follow. With only a few exceptions<label for="sn-large-polynomial" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-large-polynomial" class="margin-toggle"/><span class="sidenote">I once saw an algorithm that ran in \\(O(n(n+1)^{(4/\epsilon^2 + 1)^{2/\epsilon}})\\) time for some small \\(\epsilon > 0\\). This was argued to be efficient as \\(\epsilon\\) is a constant, so the run time is polynomial in terms of \\(n\\).</span>, most of the polynomial-time algorithms I see on a regular basis run in a much shorter amount of time, typically \\(O(n^3)\\) at most.

<h2>What about Deutsch-Jozsa?</h2>

The Deutsch-Jozsa algorithm provides a problem that quantum computers can solve in polynomial time but would take exponential time to be solved using a classical computer. The problem relies on the use of oracles, \\(f\\) for classical computers, and \\(O_f\|x\rangle\|y\rangle = \|x\rangle\|y \oplus f(x)\rangle\\) for quantum computers. Hence we have that \\(\text{EQP}\\) and \\(\text{P}\\) are different relative to these oracles. Simon's problem provides a similar relation for \\(\text{BQP}\\) and \\(\text{BPP}\\).<label for="sn-deutsch-jozsa-simon" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-deutsch-jozsa-simon" class="margin-toggle"/><span class="sidenote">For more information on either of these algorithms, Wikipedia is a good starting point: <a href="https://en.wikipedia.org/wiki/Deutsch%E2%80%93Jozsa_algorithm" target="blank">Deutsch-Jozsa algorithm</a>, <a href="https://en.wikipedia.org/wiki/Simon's_problem" target="blank">Simon's problem</a>.</span>

While relative oracles don't give us a formal proof that \\(\text{EQP} \neq \text{P}\\), they can provide us with some intuition about the relations between these complexity classes.

<h2>Closing notes</h2>

Another interesting question I was asked during the presentation was why are problems such as \\(\text{P}\\) vs \\(\text{NP}\\) so hard, particularly since the problem itself seems intuitive enough. I would like to answer this question at some point, but I feel it is a large enough question that it deserves its own blog post if I ever try to answer it.

You might notice that I include a few comics in this presentation. This was because one of my favourite comic artists, Zach Weinersmith, wrote a comic on the \\(\text{P}\\) vs \\(\text{NP}\\) problem shortly after I started putting these slides together, and I felt this was too good an opportunity to waste. Zach also made <a href="http://www.smbc-comics.com/?id=3954" target="blank">this comic</a> a few days before my presentation, but I couldn't find a suitable spot to include it. Besides, one SMBC comic is probably enough for my first long presentation.
