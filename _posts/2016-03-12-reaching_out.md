---
layout: post
section-type: post
title: Reaching Out
category: Outreach
tags: [ 'outreach', 'presentations', 'maze crawler', 'quantum playground' ]
---

After I started this PhD, one of my goals was to become more involved with outreach and education<label for="sn-other-goals" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-other-goals" class="margin-toggle"/><span class="sidenote">Other goals included more reading and to generally live healthier than I did during my undergraduate degree. Progress has been made into those as well, but that is better left for another post.</span>. Well, given that I recently took part in three science outreach events over two weeks, I think I can safely declare that this goal has been a success.

The three events in question are detailed below, along with any online materials that were used during the events and future plans I have with them.

<h2>Science in the Cinema</h2>

<a href="http://www.bristol.ac.uk/quantum-engineering/programme/year-1/science-in-the-cinema/" target="blank">Science in the Cinema</a> was an event organised by the excellent <a href="http://nicharrigan.com/" target="blank">Nicholas Harrigan</a><label for="sn-nic-harrigan" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-nic-harrigan" class="margin-toggle"/><span class="sidenote">Nic also organised for us to perform some <a href="http://www.bristol.ac.uk/quantum-engineering/programme/year-1/busking/" target="blank">science busking</a> at the @Bristol Science Centre.</span> and the University of Bristol Quantum Engineering Centre for Doctoral Training (CDT). The idea behind the event was to explore the use of science in film: From good science -- yes, there was <em>some</em> good science -- to bad science and in some cases just asking the question "How could we do this in the real world?"

To explore this subject, each student was asked to pick a film of their choice and find some aspect of the film related to science. We then prepared a 5 minute presentation on that piece of science in the film, including a live or recorded demonstration. Finally, after months of work and toil, we gave our presentations at a sold out event in the <a href="http://www.cubecinema.com/programme/event/science-in-the-cinema-screening-of-gattaca,8398/" target="blank">Cube Cinema</a>.

Subjects ranged from explaining angular momentum to asking "How can we teleport?" But being both a Computer Scientist and a Pixar fan, I couldn't help but make mine about the film WALL-E and talk about the world of robotics. My talk was specifically on the navigation of unknown terrain, and involved presenting while trapped in a maze so large all I needed to do to hide from the audience was sit down.

<img src="/maze_crawler/sinc_maze.jpg">

I also used this presentation as a reason to learn a little bit more about web animations, so I used <a href="http://fabricjs.com/" target="blank">fabric.js</a> to create examples of a navigator trying to explore a maze using different search algorithms. The source code for this part of the presentation is available on <a href="https://github.com/djmylt/maze_crawler" target="blank">GitHub</a>, though it is largely written with a focus on this one presentation instead of being used as a general resource. I have future plans for making it more useful -- largely revolving around customising the maze through a user interface -- so if that does eventually happen I will then consider creating a gh-pages branch for the project.

There is also the potential for us to hold the event again in the summer. Certainly a lot of us in the CDT want to hold it again, and there were quite a few people who were interested in a second screening due to how quickly it sold out. So make sure to keep an eye on this space if you missed it or want to see it again.

Science in the Cinema was also highlighted in a <a  href="https://uwaterloo.ca/institute-for-quantum-computing/blog/post/diary-quantum-engineer" target="blank">guest post</a> on The University of Waterloo's Institute for Quantum Computing blog written by <a href="http://scibyte.org/" target="blank">Euan Allen</a>, another PhD student in the CDT.

<h2>Digimakers</h2>
<a href="http://www.digimakers.co.uk" target="blank">Digimakers</a> is a collection of workshops organised by the University of Bristol Merchant Venturers School of Engineering (MVSE) to teach 7-18 year olds about Computer Science and Electronics. Having known about them during my undergrad, I decided to finally volunteer this year. This was convenient for them, as they were looking for PhD students to create workshops about subjects related to their research.

So, they wanted me to create a workshop about Quantum Engineering for 7-18<label for="sn-ages" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-ages" class="margin-toggle"/><span class="sidenote">Due to some of the mathematics covered, we ended up stating that the workshop was suitable for 12+ instead of 7+, though there were some really intelligent younger children who took part anyway.</span> year olds. I suspected this wasn't going to be easy.

After looking into various ways people have taught quantum computing before now, I decided to use the <a href="http://www.quantumplayground.net/" target="blank">Quantum Computing Playground</a>, a website developed by a few engineers at Google which simulates quantum computation on 6-22 qubits in the web browser. I spent the following months creating a worksheet and three days after Science in the Cinema concluded, held the workshop with <a href="https://github.com/thomasmortensson" target="blank">Thomas Mortensson</a> assisting me.

The worksheet that was written for the workshop is available <a href="/quantum_playground/quantum_playground.pdf">here</a>, and the L<sup>A</sup>T<sub>E</sub>X source code is available on <a href="https://github.com/djmylt/quantum_playground/" target="blank">GitHub</a>. The workshop required some prior knowledge about computers -- bits and logic gates -- but otherwise started out on single qubits, then moved on to quantum gates, superpositions, multiple qubits and entanglement, before finishing off with a demonstration of <a href="https://en.wikipedia.org/wiki/Deutsch%E2%80%93Jozsa_algorithm">Deutsch's Algorithm</a>.

The next event will not be until June, but I already have a few ideas on what I want to change about the worksheet. In particular, I want to try and make Section 10 -- titled More Mathematics -- more accessible. It is definitely much harder to understand than the other sections -- as emphasised in the worksheet itself and by Thomas and me in person. But as someone who first became interested in quantum computing through the theory of it, I am very reluctant to just remove it. If anyone has any thoughts on improving this or other parts of the worksheet -- or if you think I should drop my personal bias for theory and just remove that section already -- then please let me know.

<h2>Festival of Physics</h2>
Originally this blog post was just going to be about the above two events. But then an email was sent around asking for volunteers to help out at the <a href="http://www.bristol.ac.uk/physics/public-engagement/festival-of-physics/" target="blank">Bristol Festival of Physics</a>. And since I was already planning to attend the free event, I figured I might as well help out while I was there.

The volunteering at this event was similar to the science busking at @Bristol, demonstrating simple scientific experiments that you could conduct in your own home<label for="sn-liquid-nitrogen" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-liquid-nitrogen" class="margin-toggle"/><span class="sidenote">Sure, our demonstrations weren't as impressive as the liquid nitrogen demonstrations happening on the table to our right, but good luck getting permission to do those experiments in your kitchen.</span>. The experiments were taken from the Institute of Physics' <a href="http://www.iop.org/activity/outreach/resources/activity/physics/page_39581.html" target="blank">Physics to Go</a> pack and physics.org's <a href="http://www.physics.org/marvinandmilo.asp" target="blank">Marvin and Milo</a> webcomic series.

<h2>Future Events</h2>

Alas, what would I be if I was satisfied with just those outreach events under my belt? Thankfully, other events are already being planned:

<ul class="blog-post-list">
    <li>The Quantum Engineers are planning to be part of the University of Bristol Doctorial College's <a href="http://www.bristol.ac.uk/doctoral-college/rwb/" target="blank">Research Without Borders</a> event</li>
    <li>and I'm helping organise the <a href="https://pintofscience.co.uk/team/Bristol+Team" target="blank">Bristol Pint of Science Festival</a>.</li>
</ul>

There are almost certainly going to be other events in the future as well. I guess it is true that there ain't no rest for the wicked. Or in this case, the educational.

To finish where we started, and on a slightly comical note, here is another picture of me at Science in the Cinema, dressed as the Monolith from 2001: A Space Odyssey and demonstrating brainwave interference. Sadly, the conclusion of that presentation was that interfering with brainwaves via transmission of radio frequencies would not be an effective method for educating others.

<img src="/maze_crawler/sinc_monolith.png">
